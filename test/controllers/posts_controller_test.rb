require 'test_helper'

class Api::V1::PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
  end

  def authenticated_header
    hmac_secret = Rails.application.secrets.hmac_secret
    token = JWT.encode({ user_id: 1 }, hmac_secret, 'HS256')
    { 'Authorization': "Bearer #{token}" }
  end

  def authenticated_header_u23
    hmac_secret = Rails.application.secrets.hmac_secret
    token = JWT.encode({ user_id: 23 }, hmac_secret, 'HS256')
    { 'Authorization': "Bearer #{token}" }
  end

  def bad_authenticated_header
    hmac_secret = 'bad'
    token = JWT.encode({ user_id: 1 }, hmac_secret, 'HS256')
    { 'Authorization': "Bearer #{token}" }
  end

  test 'should get index' do
    get(api_v1_posts_url, as: :json)
    assert_response :success
  end

  test 'should create post' do
    assert_difference('Post.count') do
      params = {
        post: {
          body: @post.body,
          title: @post.title,
          user_id: @post.user_id
        }
      }
      post(
        api_v1_posts_url,
        params: params,
        headers: authenticated_header,
        as: :json
      )
    end

    assert_response 201
    assert JSON.parse(response.body)['user_id'] == 1
  end

  test 'should show post' do
    get api_v1_post_url(@post), as: :json
    assert_response :success
  end

  test 'should update post' do
    params = {
      post: {
        body: @post.body,
        title: @post.title,
        user_id: @post.user_id
      }
    }
    patch(
      api_v1_post_url(@post),
      headers: authenticated_header,
      params: params,
      as: :json
    )
    assert_response 200
  end

  test 'should destroy post' do
    assert_difference('Post.count', -1) do
      delete(api_v1_post_url(@post), headers: authenticated_header, as: :json)
    end
    assert_response 204
  end

  test 'should not destroy post without token' do
    delete(api_v1_post_url(@post), as: :json)
    assert_response 401
  end

  test 'should not destroy post with bad token' do
    delete(api_v1_post_url(@post), headers: bad_authenticated_header, as: :json)
    assert_response 401
  end

  test 'should not destroy post with wrong user' do
    delete(api_v1_post_url(@post), headers: authenticated_header_u23, as: :json)
    assert_response 401
  end
end
